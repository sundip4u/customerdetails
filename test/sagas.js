import test from 'tape'

import { put, call, select } from 'redux-saga/effects'
import { getAllCustomers, checkout } from '../src/sagas'
import { api } from '../src/services'
import * as actions from '../src/actions'
import { getAdress } from '../src/reducers'

const customers = [1],
  adress = [1] // dummy values
const state = { customers, adress }
const getState = () => state

test('getCustomers Saga test', function (t) {
  const generator = getAllCustomers(getState)

  let next = generator.next(actions.getAllCustomers())
  t.deepEqual(next.value, call(api.getCustomers), 'must yield api.getCustomers')

  next = generator.next(customers)
  t.deepEqual(next.value, put(actions.receiveCustomers(customers)), 'must yield actions.receiveCustomers(customers)')

  t.end()
})

test('checkout Saga test', function (t) {
  const generator = checkout()

  let next = generator.next()
  t.deepEqual(next.value, select(getAdress), 'must select getAdress')

  next = generator.next(adress)
  t.deepEqual(next.value, call(api.buyCustomers, adress), 'must call api.buyCustomers(adress)')

  next = generator.next()
  t.deepEqual(next.value, put(actions.checkoutSuccess(adress)), 'must yield actions.checkoutSuccess(adress)')

  t.end()
})
