/**
 * Mocking client-server processing
 */
import _customers from './customers'

const TIMEOUT = 100
const MAX_CHECKOUT = 2 // max different items

export const api = {
  getCustomers() {
    return new Promise(resolve => {
      setTimeout(() => resolve(_customers), TIMEOUT)
    })
  },

  buyCustomers(adress) {
    return new Promise((resolve, reject) =>
      setTimeout(() => {
        if (Object.keys(adress.quantityById).length <= MAX_CHECKOUT) resolve(adress)
        else reject(`You can buy ${MAX_CHECKOUT} items at maximum in a checkout`)
      }, TIMEOUT),
    )
  },
}
