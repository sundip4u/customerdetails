export default [
  { id: 1, title: 'iPad 4 Mini', name: 'Sahay', age: 32, sex: 'male', adress: "#1234, JP Nagar 4th phase Bangalore -76", price: 500.01, inventory: 2, details: { age: 10, sex: 'male', place: 'Bangalore' } },
  { id: 2, title: 'H&M T-Shirt White', name: 'Prem', age: 30, sex: 'male', adress: "#3456, Mahadevpura, 6th phase Bangalore -67", price: 10.99, inventory: 10, details: { age: 5, sex: 'female', place: 'Chennai' } },
  { id: 3, title: 'Charli XCX - Sucker CD', name: 'Vir', age: 20, sex: 'female', adress: "#2345,Jayanagar Nagar 4th block Bangalore -05", price: 19.99, inventory: 5, details: { age: 20, sex: 'male', place: 'Delhi' } },
  { id: 4, title: 'Maruti Suzuki', price: 109, name: 'Bheem', age: 45, sex: 'female', adress: "#098,Sahakara Nagar C block Bangalore -92", inventory: 6, details: { age: 20, sex: 'male', place: 'Delhi' } },
]
