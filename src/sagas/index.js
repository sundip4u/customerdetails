/* eslint-disable no-constant-condition */

import { take, put, call, fork, select, takeEvery, all } from 'redux-saga/effects'
import * as actions from '../actions'
import { getAdress } from '../reducers'
import { api } from '../services'

export function* getAllCustomers() {
  const customers = yield call(api.getCustomers)
  yield put(actions.receiveCustomers(customers))
}

export function* checkout() {
  try {
    const adress = yield select(getAdress)
    yield call(api.buyCustomers, adress)
    yield put(actions.checkoutSuccess(adress))
  } catch (error) {
    yield put(actions.checkoutFailure(error))
  }
}

export function* watchGetCustomers() {
  /*
    takeEvery will fork a new `getAllCustomers` task on each GET_ALL_CUSTOMERS actions
    i.e. concurrent GET_ALL_CUSTOMERS actions are allowed
  */
  yield takeEvery(actions.GET_ALL_CUSTOMERS, getAllCustomers)
}

export function* watchCheckout() {
  while (true) {
    yield take(actions.CHECKOUT_REQUEST)
    /*
      ***THIS IS A BLOCKING CALL***
      It means that watchCheckout will ignore any CHECKOUT_REQUEST event until
      the current checkout completes, either by success or by Error.
      i.e. concurrent CHECKOUT_REQUEST are not allowed
      TODO: This needs to be enforced by the UI (disable checkout button)
    */
    yield call(checkout)
  }
}

export default function* root() {
  yield all([fork(getAllCustomers), fork(watchGetCustomers), fork(watchCheckout)])
}
