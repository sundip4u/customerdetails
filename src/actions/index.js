export const GET_ALL_CUSTOMERS = 'GET_ALL_CUSTOMERS'
export const RECEIVE_CUSTOMERS = 'RECEIVE_CUSTOMERS'

export const ADD_TO_ADRESS = 'ADD_TO_ADRESS'
export const REMOVE_FROM_ADRESS = 'REMOVE_FROM_ADRESS'

export const CHECKOUT_REQUEST = 'CHECKOUT_REQUEST'
export const CHECKOUT_SUCCESS = 'CHECKOUT_SUCCESS'
export const CHECKOUT_FAILURE = 'CHECKOUT_FAILURE'

export function getAllCustomers() {
  return {
    type: GET_ALL_CUSTOMERS,
  }
}

export function receiveCustomers(customers) {
  return {
    type: RECEIVE_CUSTOMERS,
    customers: customers,
  }
}

export function addToAdress(customerId) {
  return {
    type: ADD_TO_ADRESS,
    customerId,
  }
}



export function checkout() {
  return {
    type: CHECKOUT_REQUEST,
  }
}

export function checkoutSuccess(adress) {
  return {
    type: CHECKOUT_SUCCESS,
    adress,
  }
}

export function checkoutFailure(error) {
  return {
    type: CHECKOUT_FAILURE,
    error,
  }
}
