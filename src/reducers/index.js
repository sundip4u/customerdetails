import { combineReducers } from 'redux'
import { ADD_TO_ADRESS } from '../actions'
import { default as adress, getQuantity, getAddedIds } from './adress'
import { default as customers, getCustomer } from './customers'

export function getAdress(state) {
  return state.adress
}

export function getCheckoutError(state) {
  return state.adress.checkoutStatus.error
}

export function isCheckoutPending(state) {
  return state.adress.checkoutStatus.checkoutPending
}

export function getTotal(state) {
  return getAddedIds(state.adress)
    .reduce((total, id) => total + getCustomer(state.customers, id).price * getQuantity(state.adress, id), 0)
    .toFixed(2)
}

export function getAdressCustomers(state) {
  return getAddedIds(state.adress).map(id => ({
    ...getCustomer(state.customers, id),
    quantity: getQuantity(state.adress, id),
  }))
}

const shoppingAdress = combineReducers({
  adress,
  customers,
})

export default function root(state, action) {
  if (action.type === ADD_TO_ADRESS && state.customers.byId[action.customerId].inventory <= 0) return state

  return shoppingAdress(state, action)
}
