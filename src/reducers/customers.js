import { combineReducers } from 'redux'
import { RECEIVE_CUSTOMERS, ADD_TO_ADRESS, REMOVE_FROM_ADRESS } from '../actions'

function customers(state, action) {
  switch (action.type) {
    case ADD_TO_ADRESS:
      return {
        ...state,
        inventory: state.inventory - 1,
      }

    default:
      return state
  }
}

function byId(state = {}, action) {
  switch (action.type) {
    case RECEIVE_CUSTOMERS:
      return {
        ...state,
        ...action.customers.reduce((obj, customer) => {
          obj[customer.id] = customer
          return obj
        }, {}),
      }
    default:
      const { customerId } = action
      if (customerId) {
        return {
          ...state,
          [customerId]: customers(state[customerId], action),
        }
      }
      return state
  }
}

function visibleIds(state = [], action) {
  switch (action.type) {
    case RECEIVE_CUSTOMERS:
      return action.customers.map(customer => customer.id)
    default:
      return state
  }
}

export default combineReducers({
  byId,
  visibleIds,
})

export function getCustomer(state, id) {
  return state.byId[id]
}

export function getVisibleCustomers(state) {
  return state.visibleIds.map(id => getCustomer(state, id))
}
