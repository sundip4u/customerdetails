import { combineReducers } from 'redux'
import { ADD_TO_ADRESS, CHECKOUT_REQUEST, CHECKOUT_SUCCESS, CHECKOUT_FAILURE } from '../actions'

const initialState = {
  checkoutStatus: {
    checkoutPending: false,
    error: null,
  },
  quantityById: {},
}

function checkoutStatus(state = initialState.checkoutStatus, action) {
  switch (action.type) {
    case CHECKOUT_REQUEST:
      return {
        checkoutPending: true,
        error: null,
      }
    case CHECKOUT_SUCCESS:
      return initialState.checkoutStatus
    case CHECKOUT_FAILURE:
      return {
        checkoutPending: false,
        error: action.error,
      }
    default:
      return state
  }
}

function quantityById(state = initialState.quantityById, action) {
  const { customerId } = action
  switch (action.type) {
    case CHECKOUT_SUCCESS:
      return initialState.quantityById
    case ADD_TO_ADRESS:
      return {
        [customerId]: (state[customerId] || 0),
      }

    default:
      return state
  }
}

export default combineReducers({
  checkoutStatus,
  quantityById,
})

export function getQuantity(state, customerId) {
  return state.quantityById[customerId] || 0
}

export function getAddedIds(state) {
  return Object.keys(state.quantityById)
}
