import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Details from './Details'

export default class AdressItem extends Component {
  render() {
    const { name } = this.props

    return (

      <Details name={name} />
    )
  }
}

AdressItem.propTypes = {

  name: PropTypes.string,

}
