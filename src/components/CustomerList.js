import React, { Component } from 'react'
import PropTypes from 'prop-types'
import CustomerItem from './CustomerItem'

import { connect } from 'react-redux'
import { addToAdress } from '../actions'
import { getVisibleCustomers } from '../reducers/customers'

class CustomerList extends Component {
  render() {
    const { customers, addToAdress } = this.props

    return (
      <div className="left-panel">
        <h3>Customers</h3>
        {customers.map(customer => (
          <CustomerItem adress={customer.adress} id={customer.id} customer={customer} onAddToAdressClicked={() => addToAdress(customer.id)} />
        ))}
      </div>
    )
  }
}

CustomerList.propTypes = {
  customers: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      adress: PropTypes.string.isRequired,
    }),
  ).isRequired,
  addToAdress: PropTypes.func.isRequired,
}

export default connect(
  state => ({ customers: getVisibleCustomers(state.customers) }),
  { addToAdress },
)(CustomerList)
