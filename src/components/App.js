import React, { Component } from 'react'
import CustomerList from './CustomerList'
import Adress from './Adress'

export default class App extends Component {
  render() {
    return (
      <div className="customer-details">
        <h2>Customer Details</h2>
        <hr />
        <CustomerList />

        <Adress />
      </div>
    )
  }
}
