import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Customer extends Component {
  render() {
    const { action, name, id, age, sex } = this.props
    return (

      <div>

        <div className="divDetails">
          <span className='cName'>{name}   Employee ID: {id}</span>
          <span className='cAge'> age: {age} - sex: {sex} </span>
        </div >
        <div className="divAction"> {action} </div>

      </div>
    )
  }
}

Customer.propTypes = {
  price: PropTypes.number,
  quantity: PropTypes.number,
  title: PropTypes.string,
  action: PropTypes.node,
  name: PropTypes.string,
  adress: PropTypes.string,
}
