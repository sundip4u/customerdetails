import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Customer from './Customer'

export default class CustomerItem extends Component {
  render() {
    const { customer } = this.props
    const addToAdressAction = (
      <button onClick={this.props.onAddToAdressClicked}>
        {customer.adress ? 'Show Adress' : 'No Adress Present'}
      </button>
    )

    return (
      <div style={{ marginBottom: 20 }}>
        <Customer age={customer.age} id={customer.id} adress={customer.adress} sex={customer.sex} title={customer.title} name={customer.name} price={customer.price} action={addToAdressAction} />
      </div>
    )
  }
}

CustomerItem.propTypes = {
  customer: PropTypes.shape({
    name: PropTypes.string.isRequired,
    adress: PropTypes.string.isRequired
  }).isRequired,
  onAddToAdressClicked: PropTypes.func.isRequired,
}
