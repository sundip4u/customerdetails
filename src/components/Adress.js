import React, { Component } from 'react'
import PropTypes from 'prop-types'
import AdressItem from './AdressItem'
import { connect } from 'react-redux'
import { getAdressCustomers } from '../reducers'

class AdressDetail extends Component {
  render() {
    const { customers } = this.props


    const adress1 =
      customers.map(customer => (
        <AdressItem
          name={customer.adress}
        />
      ))


    return (
      <div className='right-panel'>
        <h3>Adress of </h3>
        <div>{adress1}</div>
      </div>
    )
  }
}

AdressDetail.propTypes = {
  // data
  customers: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      adress: PropTypes.string.isRequired,
    }),
  ).isRequired,



}

export default connect(
  state => ({
    customers: getAdressCustomers(state),
  }),

)(AdressDetail)
